#include "ofApp.h"
#include <cmath>

Point::Point(float x, float y){
    circle.setPosition(x,y,0);
	
    circle.setRadius(20);
}
//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    spawn = true;
    deletePoint = false;
    circle.setPosition(ofGetWidth()/2,ofGetHeight()/2,0);
	ofEnableAlphaBlending();
}

//--------------------------------------------------------------
void ofApp::update(){
  	int maxDist = 200;
    circle.setRadius(circle.getRadius()+30);
    if(circle.getRadius() >= ofGetWidth()){
        circle.setRadius(0);
    }
    for(int i = 0; i < points.size(); i++){
	  	float dist = 
        float r = (points[i].pos.distance(circle.getPosition()))-circle.getRadius();
        r = round(r);
		r = r > 0 ? r : -r;
		if(r < maxDist){
        	points[i].circle.setRadius(15 + 20*(1+sin(-M_PI/2 + (M_PI/maxDist)*(maxDist-r))));
		}else{
        	points[i].circle.setRadius(15);
		}
    }
}
/*
void ofApp::getDist(){
  for(int p = 0; p < points.size(); p++){
    float r = (points[i].pos.distance(circles[0].getPosition()))-circles[0].getRadius();
  }
}*/

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetColor(255,255,255,255);
    for(int i = 0; i < points.size(); i++){
        points[i].circle.draw();
    }
	ofSetColor(0,0,0,30);
    circle.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key){
        case 3682: //left control
            deletePoint = false;
            spawn = false;
            break;
        case 3680: //left caps
            spawn = false;
            deletePoint = true;
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
     switch(key){
        case 3682: //left control
            deletePoint = false;
            spawn = true;
            break;
        case 3680: //left caps
            cout<< deletePoint <<  endl;
            deletePoint = false;
            spawn = true;
            break;
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
        switch(button){
        case 0:
            if(!dragging){    
                for(int i = 0; i < points.size(); i++){
                    if(points[i].pos.distance(ofVec3f(x,y,0))<25){
                        points[i].dragged = true;
                        dragging = true;
                        for(int j = 0; j < points.size(); j++){
                            if (i == j)
                                continue;
                            points[j].dragged = false;
                        }
                        break;
                    }
                }
            }
            if(dragging){
                for(int i = 0; i < points.size(); i++){
                    if(points[i].dragged == true){
                        points[i].circle.setPosition(ofVec3f(x,y,0));
                        points[i].pos = ofVec3f(x,y,0);
                    }
                }
            }
    }
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    switch(button){
        case 0:
            if(spawn){
                Point p(x,y);
                x = float(x);
                y = float(y);
                p.pos = ofVec3f(x,y,0);
                points.push_back(p);
            }
            if (deletePoint){
                for(int i = 0; i < points.size(); i++){
                    if(points[i].pos.distance(ofVec3f(x,y,0))<25){
                        points.erase(points.begin()+i);
                    }
                }
            }
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    switch(button){
        case 0:
            for(int i = 0; i < points.size(); i++){
                points[i].dragged = false;
            }
            dragging = false;
    }
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
